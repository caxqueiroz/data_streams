package sampling

import scala.util.Random

/**
 * User: cq
 * Date: 14/10/13
 * Time: 6:36 PM
 */
trait Sampling {

  def computeArray(size: Int) : Array[Int] = {
    val rnd = new Random()
    val arr = new Array[Int](size)
    arr.map(i => rnd.nextInt(size))
  }

  def printArray(arr:Array[Int]) = {
    arr.map(i => print(i + " "))
    println()
  }
}
