package sampling

import scala.util.Random

/**
 * User: cq
 * Date: 14/10/13
 * Time: 10:41 AM
 */
object Reservoir extends Sampling {


	def reservoirImp(n: Int, ds:Array[Int]) = {
		val rs = new Array[Int](n)

		ds.zipWithIndex.map{case (s,i) => if(i < n) rs(i) = s}

		val N = ds.size
		val rnd = new Random()

		for (i <- n to N - 1){
			val rn = rnd.nextDouble()
			val m = (rn * i).asInstanceOf[Int]
			if(m < n){
				rs(m) = ds(i)
			}
		}
		printArray(rs)

	}

	def main(args : Array[String]) {
		// size of reservoir
		val n = 5
//		val ds = Array(2,3,4,5,9,11,2,20,55,89,133,212,90282,82927,1212,3434,7383,93837,1,9920)
		val ds = computeArray(200)
		printArray(ds)
		for (i <- 1 to 200)
			reservoirImp(n,ds)

	}



}
