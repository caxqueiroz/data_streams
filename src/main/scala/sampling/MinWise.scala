package sampling

import scala.util.Random

/**
 * User: cq
 * Date: 14/10/13
 * Time: 11:20 AM
 */
object MinWise extends Sampling{


  def minWiseImp(n: Int, ds: Array[Int]): Array[Int] = {

    val rds = Stream.continually(Random.nextDouble()).take(ds.size).toArray
    val asList = ds.zip(rds).toList
    val minWise = asList.sortBy(t => t._2).take(n).map(t => t._1)
    minWise.toArray
  }

  def main(args : Array[String]) {
    val n = 10
    val ds = computeArray(200)
    printArray(ds)
    for (i <- 1 to 10)
      printArray(minWiseImp(n,ds))
  }
}
