package samples

import org.junit._
import Assert._
import sampling.Reservoir

@Test
class ReservoirTest {

    @Test
    def testComputeArray() = {
		val size = 200
		val arr = Reservoir.computeArray(size)
		for(i <- 0 to size -1){
			print(arr(i) + " ")
		}
		println()
		Assert.assertEquals(size,arr.size)
	}

}


